# Testlink 1.9.16 Containers for Training purposes

This repository contains docker container files  for Testlink 1.9.16, which is used in training at JAMK ICT courses

* Testlink project site: at http://testlink.org/
* Testlink source code https://github.com/TestLinkOpenSourceTRMS/


### How to deploy Testlink as a Docker container:


* What is Testlink? Please [browse and read](http://www.guru99.com/testlink-tutorial-complete-guide.html)
* What is docker container? Please [browse and read](https://www.docker.com/)



### Requirements: 



* We have been using Ubuntu 16.04 server
* Install docker + docker compose tools using *apt-get*

Log in your server instance and create folder 


```bash
mkdir gitlab_stuff
cd gitlab_stuff
```


Clone current repository in your server instance (eg. myserver.home.net)


```bash
git clone https://gitlab.com/JAMKIT/Testlink-1.9.16.git
```

Startup testlink + mysql containers using command:

```bash
sudo docker-compose up
```

After download containers should be up and running

```bash
Open your browse and open address *http://myserver.home.net/testlink
```






