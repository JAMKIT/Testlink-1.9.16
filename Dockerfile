FROM ubuntu:14.04

#ENV
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2

#Install dependencies
RUN apt-get update -y -qq && \
apt-get install -y -qq apache2 php5 php5-curl wget php5-mysql php5-gd

#Install Testlink
RUN wget -q https://downloads.sourceforge.net/project/testlink/TestLink%201.9/TestLink%201.9.16/testlink-1.9.16.tar.gz && \
tar zxf testlink-1.9.16.tar.gz && \
rm testlink-1.9.16.tar.gz && \
mv testlink-1.9.16 /var/www/html/testlink

#Add custom config
COPY config_db.inc.php /var/www/html/testlink/
COPY custom_config.inc.php /var/www/html/testlink/

RUN rm -fr /var/www/html/testlink/install && \
mkdir -p $APACHE_RUN_DIR && \
mkdir -p $APACHE_LOCK_DIR && \
mkdir -p $APACHE_LOG_DIR && \
mkdir -p /var/testlink/logs && \
mkdir -p /var/testlink/upload_area && \
mkdir -p /var/testlink/database && \
chmod +777 /var/www/html/testlink && \
chmod +777 /var/testlink/logs && \
chmod +777 /var/testlink/upload_area 

EXPOSE 80/tcp

CMD ["/usr/sbin/apache2","-D", "FOREGROUND"]

